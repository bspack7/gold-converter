from django.db import models

# Create your models here.

# must store the table of conversion factors in a Model

class Converter(models.Model):
    factor = models.FloatField(default=0)
    units = models.CharField(default='t_oz', max_length=5)