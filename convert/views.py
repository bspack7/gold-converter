from django.shortcuts import redirect
from .models import Converter
from django.http import JsonResponse

# Create your views here.

# create a view which responds to GET requests of this form  var unitconvURL = "?from=lbs&to=t_oz&value=" + weightLbs;

# the value parameter represents how many pounds, ounces, tons, grams, etc. the user wishes to convert
# the from parameter identifies which units describe the supplied value
# the to parameter specifies into which units the user wants value converted into

# Create a view which is accessed by visiting this App's base URL + "/init" which will initialize the
# database with the neccessary Unit Conversion factors.

def converter(request):
    val = int(request.GET['value'])
    fromP = request.GET['from']
    to = request.GET['to']

    if val <= 0:
        return JsonResponse({"error": "Invalid unit conversion request"})
    if fromP != 't_oz' and fromP != 'lbs':
        return JsonResponse({"error": "Invalid unit conversion request"})
    if to != 't_oz' and to != 'lbs':
        return JsonResponse({"error": "Invalid unit conversion request"})

    conversionFac = Converter.objects.get(units='t_oz')
    computation = conversionFac.factor * val
    result = {
        'value': computation,
        'units': to
    }
    return JsonResponse(result)

def init(request):
    for o in Converter.objects.all():
        o.delete()

    c = Converter(units='t_oz',factor=14.5833)
    c.save()

    cNext = Converter(units='lbs',factor=1)
    cNext.save()

    return redirect('/')